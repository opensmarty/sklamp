#!/bin/bash
# Version: 1.0.0  
# Author: seven
# Date: 2015/1/5  
# Description: Php Install Script

if [ ! -d "/sk/server/apache" ]; then
    echo 'you dont install apache'
    exit 1
fi

lamp_dir=$(cd `dirname $0`; pwd)


rm -rf /sk/server/gd2/

#install gd2
    tar -zxvf $lamp_dir/tools/php/libgd-2.1.0.tar.gz -C $lamp_dir/tools/php | tee /sk/server/log/install/php/gd_tar.log
    cd $lamp_dir/tools/php/libgd-2.1.0/
    ./configure --prefix=/sk/server/gd2 \
    --with-jpeg=/sk/server/jpeg/ \
    --with-freetype=/sk/server/freetype/ \
    --with-png=/sk/server/libpng/ | tee /sk/server/log/install/php/gd_configure.log
    make && make install | tee /sk/server/log/install/php/gd_install.log


echo "###### php_init install completed ######"
